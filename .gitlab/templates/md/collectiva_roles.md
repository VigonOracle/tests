# Titre du rôle

## Raisons d'être

- Raison 1
- Raison 2

## Missions et redevabilité

- Mission 1
- Mission 2

## Champs d'autonomie

- Champ 1
- Champ 2

## Mandat

| Donnée | Valeur |
|:-------|:-------|
| Mandataire | Nom |
| Durée  | x an |
| Mode d'attribution | Election x |
| Date | 2020-09-23 |
